package com.example.fit.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.fit.Entities.Profile;

import java.util.ArrayList;
import java.util.List;

public class UserDAO extends SQLiteOpenHelper{



    public static final String DATABASE_NAME = "FitDB.db";
    public static final String USER_TABLE_NAME = "users";
    public static final String USER_COLUMN_NAME = "name";
    public static final String USER_COLUMN_ID = "id";
    public static final String USER_COLUMN_HEIGHT = "height";
    public static final String USER_COLUMN_AGE = "age";
    public static final String USER_COLUMN_GENDER = "gender";
    public static final String USER_COLUMN_GOAL = "goal";

    private List<Profile> listUser = new ArrayList<>();

    public UserDAO(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    // Gender : 0 for male and 1 for female
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table users " +
                        "(id integer primary key, name text,height integer,age integer, gender integer, goal text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS users");
        onCreate(db);
    }

    public boolean insertUser (String name, int height, int gender, int age, String goal) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(USER_COLUMN_NAME, name);
        contentValues.put(USER_COLUMN_AGE, age);
        contentValues.put(USER_COLUMN_GENDER, gender);
        contentValues.put(USER_COLUMN_HEIGHT, height);
        contentValues.put(USER_COLUMN_GOAL, goal);
        db.insert("users", null, contentValues);
        return true;
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from users where id="+id+"", null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, USER_TABLE_NAME);
        return numRows;
    }

    public Integer deleteUser (Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("users",
                "id = ? ",
                new String[] { Integer.toString(id) });
    }

    public void eraseData(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM users");
        db.close();
    }

    public List<Profile> getAllUser() {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from users", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){

            String name = res.getString(res.getColumnIndex(USER_COLUMN_NAME));
            int age = Integer.parseInt(res.getString(res.getColumnIndex(USER_COLUMN_AGE)));
            int height = Integer.parseInt(res.getString(res.getColumnIndex(USER_COLUMN_HEIGHT)));
            int gender = Integer.parseInt(res.getString(res.getColumnIndex(USER_COLUMN_GENDER)));
            String goal = res.getString(res.getColumnIndex(USER_COLUMN_GOAL));
            Profile user = new Profile(name, age, height, gender, goal);
            listUser.add(user);
            res.moveToNext();
        }
        return listUser;
    }
}