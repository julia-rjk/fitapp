package com.example.fit.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.fit.Entities.Weight;

import java.util.ArrayList;
import java.util.List;

public class WeightDAO extends SQLiteOpenHelper{



    public static final String DATABASE_NAME = "weightDB.db";
    public static final String WEIGHT_TABLE_NAME = "weights";
    public static final String WEIGHT_COLUMN_ID = "id";
    public static final String WEIGHT_COLUMN_WEIGHT = "weight";
    public static final String WEIGHT_COLUMN_DATE = "date";

    private List<Weight> listWeight = new ArrayList<>();

    public WeightDAO(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    // Gender : 0 for male and 1 for female
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table weights " +
                        "(id integer, weight real, date text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS weights");
        onCreate(db);
    }

    public boolean insertWeight (int id, float weight, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        if(getData(date).getCount()>0){
            deleteWeight(date);
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(WEIGHT_COLUMN_ID, id);
        contentValues.put(WEIGHT_COLUMN_WEIGHT, weight);
        contentValues.put(WEIGHT_COLUMN_DATE, date);
        db.insert("weights", null, contentValues);
        return true;
    }

    public Cursor getData(String date) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from weights where date='"+"'", null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, WEIGHT_TABLE_NAME);
        return numRows;
    }

    public Integer deleteWeight (String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("weights",
                "date = ? ",
                new String[] { date });
    }

    public void eraseData(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM weights");
        db.close();
    }

    public List<Weight> getAllWeight() {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from weights", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){

            float weight = Float.parseFloat(res.getString(res.getColumnIndex(WEIGHT_COLUMN_WEIGHT)));
            String date = res.getString(res.getColumnIndex(WEIGHT_COLUMN_DATE));
            Weight w = new Weight(0,weight, date);
            listWeight.add(w);
            res.moveToNext();
        }
        return listWeight;
    }
}