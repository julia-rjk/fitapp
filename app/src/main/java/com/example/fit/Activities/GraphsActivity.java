package com.example.fit.Activities;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.fit.R;
import com.example.fit.Entities.Weight;
import com.example.fit.Database.WeightDAO;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static java.lang.Math.round;

public class GraphsActivity extends AppCompatActivity {
    // SW Stands for Starting Weight, CW for Current Weight

    float weight, sw, cw, weightLost;
    SharedPreferences pref;
    TextView textNbSW, textNbWeightLost, textNbCW;
    LineGraphSeries<DataPoint> series;
    GraphView graph;
    WeightDAO weightDAO;
    List<Weight> listWeights;
    SimpleDateFormat sdf;
    SimpleDateFormat todayDate;
    Calendar calendar;
    String date;
    Weight startingWeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graphics);
        pref = getSharedPreferences("user_details", MODE_PRIVATE);

        weightDAO = new WeightDAO(this);
        listWeights = weightDAO.getAllWeight();
        startingWeight = listWeights.get(0);
        sw = listWeights.get(0).getWeight();
        cw = listWeights.get(listWeights.size()-1).getWeight();

        graph = findViewById(R.id.graph);

        Button buttonReset = findViewById(R.id.buttonReset);
        Button backButton = findViewById(R.id.backButton);
        Button buttonAdd = findViewById(R.id.buttonAdd);

        // TextView
        textNbWeightLost = findViewById(R.id.textViewNbKgLost);
        textNbCW = findViewById(R.id.textViewNbCW);
        textNbSW = findViewById(R.id.textViewNbSW);
        textNbSW.setText(" " + sw);


        graph.setVisibility(View.VISIBLE);

        // set manual X bounds
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(1);
        graph.getViewport().setMaxX(7);

        // set manual Y bounds
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(cw - 20);
        graph.getViewport().setMaxY(cw + 20);


        // enable scaling and scrolling
        graph.getViewport().setScalable(false);
        graph.getViewport().setScalableY(false);
        graph.getViewport().setScrollable(true); // enables horizontal scrolling
        sdf = new SimpleDateFormat("dd-MM-yyyy");
        date = sdf.format(new Date());

        updateGraph();


        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    weightDAO.eraseData();
                    weightDAO.insertWeight(0,cw,date);
                    updateGraph();
            }
        });

        //Add total weight lost

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(GraphsActivity.this, InfoActivity.class);
                startActivity(intent);
                finish();

            }
        });

        // Date
        calendar = Calendar.getInstance();
        todayDate = new SimpleDateFormat("dd-MM-yyyy");

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogAddWeight();
                updateGraph();
            }

        });


        displayWeightLost();


    }


    public void updateGraph(){
        graph.removeAllSeries();
        int dayWeight = 0;
//        Date dayWeight = null;
//        try {
//            dayWeight = sdf.parse(startingWeight.getDate());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

        series = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(dayWeight, startingWeight.getWeight())
        });

        if(listWeights.size()>1) {
            for (int i = 1; i < listWeights.size(); i++) {
//                try {
//                    dayWeight = sdf.parse(listWeights.get(i).getDate());
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
                dayWeight++;
                series.appendData(new DataPoint(dayWeight, listWeights.get(i).getWeight()), true, 40);
            }
        }
        graph.addSeries(series);

    }

    public void dialogAddWeight(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Today's weight");

        // Set up the input
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                weight = Float.parseFloat(input.getText().toString());
                weightDAO.insertWeight(0,weight, todayDate.format(new Date()));
                displayWeightLost();
                updateGraph();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }


    public void displayWeightLost(){
        cw = listWeights.get(listWeights.size()-1).getWeight();
        DecimalFormat df = new DecimalFormat("###.#");
        weightLost = sw - cw;
        textNbCW.setText(""+cw);
        if(weightLost < 0){
            weightLost = - weightLost;
            textNbWeightLost.setText(""+(df.format(weightLost))+" kgs gained.");
        }else if(weightLost >=0){
            textNbWeightLost.setText(""+df.format(weightLost)+" kgs lost.");
        }

    }

}