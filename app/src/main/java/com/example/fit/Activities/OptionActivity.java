package com.example.fit.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.fit.R;
import com.example.fit.Database.UserDAO;
import com.example.fit.Database.WeightDAO;

public class OptionActivity  extends AppCompatActivity {

    Button buttonReset;
    SharedPreferences pref;
    UserDAO userDAO;
    WeightDAO weightDAO;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.options);

        userDAO = new UserDAO(this);
        weightDAO = new WeightDAO(this);

        buttonReset = findViewById(R.id.buttonReset);

        pref = getSharedPreferences("user_details",MODE_PRIVATE);

        buttonReset.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(View v) {
                SharedPreferences.Editor editor = pref.edit();
                editor.clear();
                editor.commit();

                userDAO.eraseData();
                weightDAO.eraseData();

                Intent intent= new Intent(OptionActivity.this,MainActivity.class);
                startActivity(intent);

                finishAffinity();
            }
        });

    }



}
