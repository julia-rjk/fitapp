package com.example.fit.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.fit.R;
import com.example.fit.Database.UserDAO;
import com.example.fit.Database.WeightDAO;

import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.Math.pow;
import static java.lang.Math.round;

public class MainActivity extends AppCompatActivity {

    Button buttonOK, buttonGender;
    RadioButton radioButtonMale, radioButtonFemale;
    EditText fieldWeight, fieldHeight, fieldName,fieldAge;
    TextView phrase, phraseGender, phraseHeight, phraseWeight, phraseName,phraseAge;
    String name;
    int height, age;
    double bmi;
    float weight;
    AlertDialog.Builder alertDialogBuilder;
    Intent intentProfile;
    SharedPreferences pref;
    int gender = 0;
    UserDAO userDAO;
    WeightDAO weightDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // DAO
        userDAO = new UserDAO(this);
        weightDAO = new WeightDAO(this);

        alertDialogBuilder = new AlertDialog.Builder(this);

        //button
        buttonOK = findViewById(R.id.buttonWeigh);
        buttonGender = findViewById(R.id.buttonGender);

        //Radio Button
        radioButtonMale = findViewById(R.id.radioButtonMale);
        radioButtonFemale = findViewById(R.id.radioButtonFemale);

        //Field
        fieldWeight = findViewById(R.id.editTextWeight);
        fieldHeight = findViewById(R.id.editTextHeight);
        fieldName = findViewById(R.id.editTextName);
        fieldAge = findViewById(R.id.editTextAge);

        //TextView
        phrase = findViewById(R.id.phrase);
        phraseGender = findViewById(R.id.textGender);
        phraseHeight = findViewById(R.id.textView);
        phraseWeight = findViewById(R.id.textView2);
        phraseName = findViewById(R.id.textName);
        phraseAge = findViewById(R.id.textAge);

        //Preferences
        pref = getSharedPreferences("user_details",MODE_PRIVATE);

        //Intent
        intentProfile = new Intent(MainActivity.this, InfoActivity.class);


        // If it's not the first usage of this app then we will start the profile activity.
        if(pref.contains("name") && pref.contains("age") && pref.contains("gender") && pref.contains("weight") && pref.contains("height")){
            startActivity(intentProfile);
        }

        radioButtonFemale.setChecked(true);

        fieldName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && fieldName.getText().toString().matches("Name")) {
                    fieldName.setText("");
                }
            }
        });

        fieldAge.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && fieldAge.getText().toString().matches("Age")) {
                    fieldAge.setText("");
                }
            }
        });


    }

    public void onClick(View v) {
        final int id = v.getId();
        switch (id) {
            case R.id.radioButtonMale:
                if(!radioButtonMale.isChecked()){
                    radioButtonFemale.setChecked(true);
                }else{
                    radioButtonFemale.setChecked(false);
                }
                break;

            case R.id.radioButtonFemale:
                if(!radioButtonFemale.isChecked()){
                    radioButtonMale.setChecked(true);
                }else{
                    radioButtonMale.setChecked(false);
                }
                break;

            case R.id.buttonGender:
                if(buttonGender.getVisibility()==View.VISIBLE){
                    if(!radioButtonMale.isChecked()){
                        gender = 1;
                    }else gender = 0;

                    if(radioButtonMale.isChecked()||radioButtonFemale.isChecked()) {


                        if(!(fieldName.getText().length() == 0)
                                && !(fieldAge.getText().toString().matches(""))
                                && !(fieldName.getText().toString().matches(""))
                                && Integer.parseInt(fieldAge.getText().toString())>0
                                && !(fieldName.getText().toString().matches("Name"))
                                && !(fieldAge.getText().toString().matches("Age"))
                                && (Integer.parseInt(fieldAge.getText().toString()) <100)
                                ){

                            layoutVisibility(0, false);
                            layoutVisibility(1, true);
                            name = fieldName.getText().toString();
                            age = Integer.parseInt(fieldAge.getText().toString());

                        }else{
                            alertDialogBuilder.setMessage("Informations are wrong.\nPlease verify and correct them.").setTitle("Information");
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }


                    }

                }
                break;

            case R.id.buttonWeigh:
                if(buttonOK.getVisibility()==View.VISIBLE){

                    // We check if editText is not empty
                    if(!(fieldHeight.getText().toString().matches("")) && !(fieldWeight.getText().toString().matches(""))){

                        //Calcul of BMI
                        height = Integer.parseInt(fieldHeight.getText().toString());
                        weight = Float.parseFloat(fieldWeight.getText().toString());

                        bmi =  round(weight/pow((height*pow(10,-2)),2));
                        phrase.setText("Your height is : "+height+"cm\nYour weight is : "+weight+"kg\nYour BMI is : "+bmi+"\n");

                        layoutVisibility(1,false);
                        phrase.setVisibility(View.VISIBLE);

                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("name", name);
                        editor.putInt("height", height);
                        editor.putFloat("weight", weight);
                        editor.putFloat("sw", weight);
                        editor.putString("gender","female");
                        editor.putInt("age",age);
                        
                        editor.commit();

                        // We get the date
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        String date = sdf.format(new Date());

                        // TODO: add possibility to change goal
                        userDAO.insertUser(name,height,gender,age, "lose");
                        weightDAO.insertWeight(0,weight, date);

                        startActivity(intentProfile);
                        finish();
                    }
                }
                break;



            default:
                break;

        }
    }


    //Set visibility of different layout..
    public void layoutVisibility(int i, boolean b){

        switch(i){
            case 0:
                if(!b){
                    phraseGender.setVisibility(View.INVISIBLE);
                    radioButtonMale.setVisibility(View.INVISIBLE);
                    radioButtonFemale.setVisibility(View.INVISIBLE);
                    buttonGender.setVisibility(View.INVISIBLE);
                    fieldName.setVisibility(View.INVISIBLE);
                    phraseName.setVisibility(View.INVISIBLE);
                    phraseAge.setVisibility(View.INVISIBLE);
                    fieldAge.setVisibility(View.INVISIBLE);

                }else{
                    phraseGender.setVisibility(View.VISIBLE);
                    radioButtonMale.setVisibility(View.VISIBLE);
                    radioButtonFemale.setVisibility(View.VISIBLE);
                    buttonGender.setVisibility(View.VISIBLE);
                }
                break;
            case 1:
                if(!b){
                    fieldHeight.setVisibility(View.INVISIBLE);
                    fieldWeight.setVisibility(View.INVISIBLE);
                    phrase.setVisibility(View.INVISIBLE);
                    phraseHeight.setVisibility(View.INVISIBLE);
                    phraseWeight.setVisibility(View.INVISIBLE);
                    buttonOK.setVisibility(View.INVISIBLE);
                }else{
                    fieldHeight.setVisibility(View.VISIBLE);
                    fieldWeight.setVisibility(View.VISIBLE);
                    phrase.setVisibility(View.VISIBLE);
                    phraseHeight.setVisibility(View.VISIBLE);
                    phraseWeight.setVisibility(View.VISIBLE);
                    buttonOK.setVisibility(View.VISIBLE);
                }

                break;

            default:
                break;

        }

    }

}
