package com.example.fit.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.fit.Entities.Profile;
import com.example.fit.R;
import com.example.fit.Database.UserDAO;
import com.example.fit.Entities.Weight;
import com.example.fit.Database.WeightDAO;

import java.util.List;

import static java.lang.Math.pow;
import static java.lang.Math.round;

public class InfoActivity extends AppCompatActivity {

    Profile profile;
    String name;
    int height, bmi,age, gender;
    float weight;
    // BMR : basal metabolic rate
    double bmr;
    boolean male;
    SharedPreferences pref;

    UserDAO userDAO;
    WeightDAO weightDAO;
    List<Weight> listWeights;
    TextView  phraseGender, phraseHeight, phraseWeight, phraseBmi, phraseBmr, phraseName, phraseAge;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        //TextView
        phraseGender = findViewById(R.id.gender);
        phraseName = findViewById(R.id.Name);
        phraseHeight = findViewById(R.id.Height);
        phraseWeight = findViewById(R.id.Weight);
        phraseBmr = findViewById(R.id.bmr);
        phraseBmi = findViewById(R.id.bmi);
        phraseAge = findViewById(R.id.textAge);


        // DAO
        userDAO = new UserDAO(this);
        weightDAO = new WeightDAO(this);

        userDAO.insertUser("name",170,0,19,"lose");
        List<Profile> listUser = userDAO.getAllUser();
        listWeights = weightDAO.getAllWeight();
        weight = listWeights.get(listWeights.size()-1).getWeight();
        Profile user = listUser.get(0);
        name = user.getName();
        height = user.getHeight();
        age = user.getAge();
        gender = user.getGender();
        bmi = (int) round(weight/pow((height*pow(10,-2)),2));

        if(gender == 0){
            phraseGender.setText("Male");
            bmr = 66.5 + (13.75*weight) + ( 5.003*height)-(6.755*age);
        }else{
            bmr = 655.1 + ( 9.563*weight) + ( 1.850*height)-( 4.676 *age);
            phraseGender.setText("Female");
        }

        phraseHeight.setText(height+" cm");
        phraseWeight.setText(weight+" kg");
        phraseName.setText("Hello " + name + " !");
        phraseBmi.setText("BMI : "+bmi);
        phraseAge.setText("Age : "+age);
        phraseBmr.setText("BMR : "+ (int)bmr);

        Button buttonInformation = findViewById(R.id.buttonInformations);
        buttonInformation.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                alertDialogBuilder.setMessage("BMR : Basal Metabolic Rate\n" +
                        "Number of calories burned just by living\n\n"+
                        "BMI : Body Mass Index\n"+
                        "Underweight = <18.5\n" +
                        "Normal weight = 18.5–24.9\n" +
                        "Overweight = 25–29.9\n" +
                        "Obesity = BMI of 30 or greater\n\n" +
                        "To Lose Weight\n" +
                        "To take off one pound per week,\n" +
                        "you’ll need to reduce calories by 500 per day\n" +
                        "(1 pound of body fat equals about 3,500 calories).\n" +
                        "Try eating 250 calories less per day and exercising enough to burn 250 calories,\n" +
                        "like walking about 2.5 miles each day.\n" +
                        "The easiest way to cut back on calories is to watch your portion sizes.\n\n" +
                        "To Gain Weight\n" +
                        "Try eating about 500 calories more per day and limit your cardio activity.\n" +
                        "Focus on basic compound strength training exercises like barbell squat,\n" +
                        "bench press and barbell rows to add lean muscle mass to your physique.\n" +
                        "Make sure to eat enough protein (at least 1 gram per pound of body weight)\n" +
                        "and get plenty of rest to recover and grow after your workouts.\n\n" +
                        "Calories needed :\n" +
                        "Men need 2000 - 2200 calories\n" +
                        "Women need 1800 - 2000 calories\n").setTitle("Information");

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        Button button = findViewById(R.id.buttonStatistics);
        button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent= new Intent(InfoActivity.this, GraphsActivity.class);
                startActivity(intent);
            }
        });

        Button buttonOption = findViewById(R.id.buttonOptions);

        buttonOption.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent= new Intent(InfoActivity.this,OptionActivity.class);
                startActivity(intent);
            }
        });


    }




}
