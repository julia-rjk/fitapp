package com.example.fit.Entities;

import static java.lang.Math.pow;
import static java.lang.Math.round;

public class Profile {
    private String name, goal;
    private int height, weight;
    private int mb,age;

    private int gender;
    private double bmi;

    public Profile(String name, int age, int height, int gender, String goal){
        this.name= name;
        this.height=height;
        this.gender=gender;
        this.age = age;
        if(goal.matches("lose") ||goal.matches("gain")|| goal.matches("maintain")) this.goal = goal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getMb() {
        return mb;
    }

    public void setMb(int mb) {
        this.mb = mb;
    }

    public double getBmi() {
        return bmi;
    }

    public void setBmi(int bmi) {
        this.bmi = bmi;
    }

    public int getGender() {
        return gender;
    }

    public void setMale(int gender) {
        this.gender = gender;
    }


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public void setBmi(double bmi) {
        this.bmi = bmi;
    }


    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

}
